package no.ntnu.idatg1002.gruppe4.run;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import no.ntnu.idatg1002.gruppe4.task.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

/**
 * The New task view controller.
 */
public class NewTaskViewController implements Initializable {
    @FXML
    private Button backButton;
    @FXML
    private Button settingsButton;
    @FXML
    private Button exitButton;
    @FXML
    private TextArea taskDescriptionField;
    @FXML
    private ComboBox<String> comboBoxPriority;
    @FXML
    private ComboBox<String> comboBoxCategory;
    @FXML
    private DatePicker deadlineTaskCalendar;
    @FXML
    private TextField nameOfTaskField;
    @FXML
    private Button createTaskButton;
    @FXML
    private Label WordCountLabel;


    /**
     * Gets combo box category.
     *
     * @return the combo box category
     */
    public ComboBox getComboBoxCategory() {
        return comboBoxCategory;
    }

    /**
     * Gets combo box priority.
     *
     * @return the combo box priority
     */
    public ComboBox getComboBoxPriority() {
        return comboBoxPriority;
    }

    /**
     * Handle back button. Goes back to the main menu.
     *
     * @throws IOException the io exception
     */
    public void handleBackButton() throws IOException {
        Parent root = FXMLLoader.
                load((getClass().getClassLoader().getResource("MainMenuView.fxml")));

        Stage window = (Stage) backButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Handle exit button. Exits the program.
     */
    @FXML
    public void handleExitButton(){
        App.exitDialogue();
    }

    /**
     * Handle settings button. Opens settings.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleSettingsButton() throws IOException {
        App.previousView = "NewTaskView.fxml";

        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("SettingsView.fxml")));
        Stage window = (Stage) settingsButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Set all unique categories to combobox.
     */
    @FXML
    public void getCategories() {
        // Make an observable list with every unique category
        ObservableList<String> listOfCategories = FXCollections.observableArrayList(new ArrayList<>());
        for (String category : App.taskList.getCategories()) {
            listOfCategories.add(category);
        }
        // Set category combobox to every unique existing category
        comboBoxCategory.setItems(listOfCategories);
    }

    /**
     * Creates a new task with input from user and adds it to taskList.
     * If at least one of the input boxes are empty, nothing happens.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleCreateButton() throws IOException {
        nameOfTaskField.setStyle("-fx-border-color: lightgrey;");
        taskDescriptionField.setStyle("-fx-border-color: lightgrey;");
        comboBoxPriority.setStyle("-fx-border-color: lightgrey;");
        comboBoxCategory.setStyle("-fx-border-color: lightgrey;");
        deadlineTaskCalendar.setStyle("-fx-border-color: lightgrey;");
        if (nameOfTaskField.getText().isEmpty()) {
            nameOfTaskField.setStyle("-fx-border-color: red;");
        }
        if (taskDescriptionField.getText().isEmpty()) {
            taskDescriptionField.setStyle("-fx-border-color: red;");
        }
        if (comboBoxPriority.getValue() == null) {
            comboBoxPriority.setStyle("-fx-border-color: red;");
        }
        if (comboBoxCategory.getValue() == null) {
            comboBoxCategory.setStyle("-fx-border-color: red;");
        }
        if (deadlineTaskCalendar.getValue() == null) {
            deadlineTaskCalendar.setStyle("-fx-border-color: red;");
        }
        else if (!nameOfTaskField.getText().isEmpty() && !taskDescriptionField.getText().isEmpty() &&
                comboBoxPriority.getValue() != null && comboBoxCategory.getValue() != null &&
                deadlineTaskCalendar.getValue() != null) {
            Task task = new Task(nameOfTaskField.getText(), taskDescriptionField.getText(), comboBoxPriority.getValue()
                    , deadlineTaskCalendar.getValue(), comboBoxCategory.getValue());
            App.taskList.addTask(task);

            Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("MainMenuView.fxml")));
            Stage window = (Stage) backButton.getScene().getWindow();
            window.setScene(new Scene(root));
        }
    }

    /**
     * Creates a wordcount and sets the cap for 255 on description to avoid a fault
     * also stops the user from going over 255
     */
    public void updateWordCount(){
        WordCountLabel.textProperty().bind(taskDescriptionField.textProperty()
                .length().asString("Word count: %d / 255"));

        Pattern pattern = Pattern.compile(".{0,255}");
        TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return pattern.matcher(change.getControlNewText()).matches() ? change : null;
        });

        taskDescriptionField.setTextFormatter(formatter);
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Set only future dates
        deadlineTaskCalendar.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) < 0);
            }
        });
        // Get categories for combobox
        getCategories();
        updateWordCount();
    }
}
