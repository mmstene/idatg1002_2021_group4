package no.ntnu.idatg1002.gruppe4.run;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import no.ntnu.idatg1002.gruppe4.task.Task;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The Archived tasks view controller.
 */
public class ArchivedTasksViewController implements Initializable {

    @FXML
    public Button backButton;
    @FXML
    public Button settingsButton;
    @FXML
    public Button exitButton;
    @FXML
    public Button undoButton;
    @FXML
    private ListView listOfTasks;
    @FXML
    private TextField title;
    @FXML
    private TextField priority;
    @FXML
    private TextField category;
    @FXML
    private TextField startDate;
    @FXML
    private TextField endDate;
    @FXML
    private TextField deadline;
    @FXML
    private TextArea description;

    /**
     * The list of archived Task objects currently shown in the list view.
     */
    private List<Task> tasksCurrentlyShown;

    /**
     * Handle back button. Go back to main menu.
     *
     * @throws IOException the io exception
     */
    public void handleBackButton() throws IOException {
        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("MainMenuView.fxml")));
        Stage window = (Stage) backButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Handle exit button. Exit the application.
     */
    @FXML
    public void handleExitButton() {
        App.exitDialogue();
    }

    /**
     * Handle settings button. Opens settings.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleSettingsButton() throws IOException {
        App.previousView = "ArchivedTasksView.fxml";
        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("SettingsView.fxml")));
        Stage window = (Stage) settingsButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle undo button. Moves tasks back to active tasks.
     */
    @FXML
    public void handleUndo() {
        if(listOfTasks.getSelectionModel().getSelectedItem() != null) {
            ObservableList<Integer> indices = listOfTasks.getSelectionModel().getSelectedIndices();
            Task selectedTask;
            for(Integer index : indices) {
                // Get the task of selected index
                selectedTask = tasksCurrentlyShown.get(index);
                // Mark the task as finished
                App.taskList.unmarkAsDone(selectedTask);
            }
            updateTasks();
            undoButton.setDisable(true);
        }
    }

    /**
     * Updates global list of tasks currently shown from database. Adds every task shown to list view.
     */
    public void updateTasks() {
        // Sets the local list of tasks currently shown to all archived tasks in database
        tasksCurrentlyShown = App.taskList.getArchivedTaskList();
        // Adds all tasks to list view
        ObservableList<String> listOfTaskNames = FXCollections.observableArrayList(new ArrayList<>());
        for (Task task : tasksCurrentlyShown) {
            listOfTaskNames.add(task.getTaskName());
        }
        listOfTasks.setItems(listOfTaskNames);
    }

    /**
     * Display selected task in ViewTaskInformation.
     */
    @FXML
    void displaySelectedTaskForView() {
        if (listOfTasks.getSelectionModel().getSelectedItem() != null) {
            undoButton.setDisable(false);

            int tempIndex = listOfTasks.getSelectionModel().getSelectedIndex();
            // Get ID of Task based on tempIndex
            App.selectedIndex = tasksCurrentlyShown.get(tempIndex).getTaskIDNumber();

            Task taskToBeDisplayed = App.taskList.getTaskById(App.getSelectedIndex());
            title.setText(taskToBeDisplayed.getTaskName());
            priority.setText(taskToBeDisplayed.getTaskPriority());
            category.setText(taskToBeDisplayed.getCategory());
            startDate.setText(taskToBeDisplayed.getTaskStartDate().toString());
            endDate.setText(taskToBeDisplayed.getTaskEndDate().toString());
            deadline.setText(taskToBeDisplayed.getDeadline().toString());
            description.setText(taskToBeDisplayed.getTaskDescription());
        } else
            undoButton.setDisable(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listOfTasks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        updateTasks();
    }
}
