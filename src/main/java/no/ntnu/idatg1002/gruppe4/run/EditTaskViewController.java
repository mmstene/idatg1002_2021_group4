package no.ntnu.idatg1002.gruppe4.run;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import no.ntnu.idatg1002.gruppe4.task.Task;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

/**
 * The Edit task view controller.
 */
public class EditTaskViewController implements Initializable {

    @FXML
    private TextArea taskDescriptionFieldShort;
    @FXML
    public TextArea taskDescriptionField;

    @FXML
    public Button backButton;
    @FXML
    public Button settingsButton;
    @FXML
    public Button exitButton;
    @FXML
    public ComboBox<String> comboBoxCategory;
    @FXML
    public ComboBox<String> comboBoxPriority;
    @FXML
    public DatePicker deadlineTaskCalendar;
    @FXML
    public Button markTaskAsDoneButton;
    @FXML
    public Button updateTaskButton;
    @FXML
    private Label WordCountLabel;

    /**
     * Gets combo box category.
     *
     * @return the combo box category
     */
    @FXML
    public ComboBox getComboBoxCategory() {
        return comboBoxCategory;
    }

    /**
     * Gets combo box priority.
     *
     * @return the combo box priority
     */
    @FXML
    public ComboBox getComboBoxPriority() {
        return comboBoxPriority;
    }

    /**
     * Handle back button. Returns to MainView.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleBackButton() throws IOException {
        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("MainMenuView.fxml")));
        Stage window = (Stage) backButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Handle exit button. Exits application.
     */
    @FXML
    public void handleExitButton(){
        App.exitDialogue();
    }

    /**
     * Handle settings button. Opens settings.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleSettingsButton() throws IOException {
        App.previousView = "EditTaskView.fxml";
        Parent root = FXMLLoader.
                load((getClass().getClassLoader().getResource("SettingsView.fxml")));
        Stage window = (Stage) settingsButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle update task button. Updates a task with changes made.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleUpdateTaskButton() throws IOException {
        if(!taskDescriptionFieldShort.getText().isEmpty() && !taskDescriptionField.getText().isEmpty() &&
            comboBoxPriority.getValue() != null && comboBoxCategory.getValue() != null &&
            deadlineTaskCalendar.getValue() != null)
        {
            // Get task object to be changed
            Task tempTask = App.taskList.getTaskById(App.getSelectedIndex());
            // Apply changes to task
            tempTask.setCategory(comboBoxCategory.getValue());
            tempTask.changePriority(comboBoxPriority.getValue());
            tempTask.setTaskName(taskDescriptionFieldShort.getText());
            tempTask.setTaskDescription(taskDescriptionField.getText());
            tempTask.setDeadline(deadlineTaskCalendar.getValue());
            // Update task in database
            App.taskList.updateTask(tempTask);
            // Return to main menu
            Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("MainMenuView.fxml")));
            Stage window = (Stage) markTaskAsDoneButton.getScene().getWindow();
            window.setScene(new Scene(root));
        }
    }

    /**
     * Handle mark as done button. Marks a task as done.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleMarkAsDoneButton() throws IOException {
        App.taskList.markAsDone(App.taskList.getTaskById(App.getSelectedIndex()));
        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("MainMenuView.fxml")));
        Stage window = (Stage) markTaskAsDoneButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Updates the category combobox with every unique category in database.
     */
    @FXML
    public void getCategories() {
        // Make an observable list with every unique category
        ObservableList<String> listOfCategories = FXCollections.observableArrayList(new ArrayList<>());
        for (String category : App.taskList.getCategories()) {
            listOfCategories.add(category);
        }
        // Set category combobox to every unique existing category
        comboBoxCategory.setItems(listOfCategories);
    }
    /**
     * Creates a wordcount and sets the cap for 255 on description to avoid a fault
     * also stops the user from going over 255
     */
    public void updateWordCount(){
        WordCountLabel.textProperty().bind(taskDescriptionField.textProperty()
                .length().asString("Word count: %d / 255"));
        Pattern pattern = Pattern.compile(".{0,255}");
        TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return pattern.matcher(change.getControlNewText()).matches() ? change : null;
        });
        taskDescriptionField.setTextFormatter(formatter);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Set categories for combobox
        getCategories();
        updateWordCount();
        // Get task to be edited
        Task editedTask = App.taskList.getTaskById(App.getSelectedIndex());
        // Display task info in GUI
        taskDescriptionFieldShort.setText(editedTask.getTaskName());
        taskDescriptionField.setText(editedTask.getTaskDescription());
        comboBoxPriority.getSelectionModel().select(editedTask.getTaskPriority());
        comboBoxCategory.getSelectionModel().select(editedTask.getCategory());
        deadlineTaskCalendar.setValue(editedTask.getDeadline());
        // Set only future dates
        deadlineTaskCalendar.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();
                setDisable(empty || date.compareTo(today) < 0);
            }
        });
    }
}

