package no.ntnu.idatg1002.gruppe4.task;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;

import java.time.LocalDate;
import java.util.List;

/**
 * The class represents a task list with database implementation
 */
public class TaskListDaoDB implements TaskList {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    /**
     * Instantiates a new Task list.
     *
     * @param testing variable to see if the application is in test mode or not
     */
    public TaskListDaoDB(boolean testing) {
        if(!testing) {
            this.emf = Persistence.createEntityManagerFactory("todo-pu");
        }
        else {
            this.emf = Persistence.createEntityManagerFactory("todo-pu-test");
        }
        this.em = emf.createEntityManager();
    }

    @Override
    public void addTask(Task task) {
        this.em.getTransaction().begin();
        this.em.persist(task);
        this.em.getTransaction().commit();
    }

    @Override
    public void removeTask(Task task) {
        this.em.getTransaction().begin();
        this.em.remove(task);
        this.em.getTransaction().commit();
    }

    @Override
    public void updateTask(Task task) {
        this.em.getTransaction().begin();
        this.em.merge(task);
        this.em.getTransaction().commit();
    }

    @Override
    public void markAsDone(Task task) {
        this.em.getTransaction().begin();
        task.setIsTaskFinished(true);
        task.setTaskEndDate(LocalDate.now());
        this.em.merge(task);
        this.em.getTransaction().commit();
    }

    @Override
    public void unmarkAsDone(Task task) {
        this.em.getTransaction().begin();
        task.setIsTaskFinished(false);
        task.setTaskEndDate(null);
        this.em.merge(task);
        this.em.getTransaction().commit();
    }

    @Override
    public void purgeAll() {
        this.em.getTransaction().begin();
        String jpql = "DELETE FROM Task t";
        this.em.createQuery(jpql).executeUpdate();
        this.em.getTransaction().commit();
    }

    @Override
    public Task getTaskById(int id) {
        String jpql = "SELECT t FROM Task t WHERE t.taskIDNumber=:id";
        Query query = this.em.createQuery(jpql).setParameter("id", id);
        return (Task) query.getResultList().get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCategories() {
        String jpql = "SELECT DISTINCT t.category FROM Task t WHERE t.isTaskFinished = false";
        Query query = this.em.createQuery(jpql);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> getActiveTaskList() {
        String jpql = "SELECT t FROM Task t WHERE t.isTaskFinished = false";
        Query query = this.em.createQuery(jpql);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> getArchivedTaskList() {
        String jpql = "SELECT t FROM Task t WHERE t.isTaskFinished = true ORDER BY t.taskEndDate DESC";
        Query query = this.em.createQuery(jpql);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> getTasksFilteredByPriority(String priority) {
        String jpql = "SELECT t FROM Task t WHERE t.taskPriority=:priority AND t.isTaskFinished = false";
        Query query = this.em.createQuery(jpql).setParameter("priority", priority);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> getTasksFilteredByCategory(String category) {
        String jpql = "SELECT t FROM Task t WHERE t.category=:category AND t.isTaskFinished = false";
        Query query = this.em.createQuery(jpql).setParameter("category", category);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> getTasksByFilter(String priority, String category) {
        String jpql = "SELECT t FROM Task t WHERE t.category=:category AND t.taskPriority=:priority AND t.isTaskFinished = false";
        Query query = this.em.createQuery(jpql).setParameter("category", category).setParameter("priority", priority);
        return query.getResultList();
    }

    @Override
    public void close() {
        this.em.close();
        this.emf.close();
    }
}
