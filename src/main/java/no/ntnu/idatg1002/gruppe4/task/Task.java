package no.ntnu.idatg1002.gruppe4.task;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import java.time.LocalDate;

/**
 * This class represents a task.
 * Tasks are uniquely identified by their IDs.
 */
@Entity
public class Task {
    @SuppressWarnings({"UnusedDeclaration"})
    @Id
    @GeneratedValue
    private int taskIDNumber;
    private String taskName;
    private String taskDescription;
    private String taskPriority; // High/Medium/Low.
    private LocalDate taskStartDate;
    private LocalDate taskEndDate;
    private LocalDate deadline;
    private String category;
    private Boolean isTaskFinished; // A task is either active (false) or finished (true).


    /**
     * This constructor should not be used, but need to be here
     */
    public Task() {  }

    /**
     * Instantiates a new Task.
     *
     * @param taskName        the task name
     * @param taskDescription the task description
     * @param taskPriority    the task priority
     * @param deadline        the deadline
     * @param category        the category
     */
    public Task(String taskName, String taskDescription, String taskPriority,
                LocalDate deadline, String category) {

        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.taskPriority = taskPriority;
        this.deadline = deadline;
        this.category = category;
        // A task starts when created, and is always not finished on creation
        this.taskStartDate = LocalDate.now();
        this.taskEndDate = null;
        this.isTaskFinished = false;
    }

    /**
     * Sets task id number.
     *
     * @param taskIDNumber the task id number
     */
    public void setTaskIDNumber(int taskIDNumber) {
        this.taskIDNumber = taskIDNumber;
    }

    /**
     * Sets task priority.
     *
     * @param taskPriority the task priority
     */
    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    /**
     * Gets task start date.
     *
     * @return the task start date
     */
    public LocalDate getTaskStartDate() {
        return taskStartDate;
    }

    /**
     * Sets task start date.
     *
     * @param taskStartDate the task start date
     */
    public void setTaskStartDate(LocalDate taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    /**
     * Gets task end date.
     *
     * @return the task end date
     */
    public LocalDate getTaskEndDate() {
        return taskEndDate;
    }

    /**
     * Gets task finished.
     *
     * @return the task finished
     */
    public Boolean getTaskFinished() {
        return isTaskFinished;
    }

    /**
     * Sets task finished.
     *
     * @param taskFinished the task finished
     */
    public void setTaskFinished(Boolean taskFinished) {
        isTaskFinished = taskFinished;
    }

    /**
     * Gets task id number.
     *
     * @return the task id number
     */
    public int getTaskIDNumber() {
        return taskIDNumber;
    }

    /**
     * Gets task name.
     *
     * @return the task name
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Sets task name.
     *
     * @param taskName the task name
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * Gets task description.
     *
     * @return the task description
     */
    public String getTaskDescription() {
        return taskDescription;
    }

    /**
     * Sets task description.
     *
     * @param taskDescription the task description
     */
    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    /**
     * Gets task priority.
     *
     * @return the task priority
     */
    public String getTaskPriority() {
        return this.taskPriority;
    }

    /**
     * Gets task priority as a number for sorting purposes.
     *
     * @return the task priority numeric
     */
    public int getTaskPriorityNumeric() {
        return switch (this.taskPriority) {
            case "Low" -> 1;
            case "Medium" -> 2;
            case "High" -> 3;
            default -> 0;
        };
    }

    /**
     * Change priority of task.
     *
     * @param priority the priority
     */
    public void changePriority(String priority) {
        this.taskPriority = priority;
    }

    /**
     * Sets task end date.
     *
     * @param taskEndDate the task end date
     */
    public void setTaskEndDate(LocalDate taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    /**
     * Gets deadline.
     *
     * @return the deadline
     */
    public LocalDate getDeadline() {
        return deadline;
    }

    /**
     * Sets deadline.
     *
     * @param deadline the deadline
     */
    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Sets task status to done or not done
     *
     * @param isTaskFinished is task finished
     */
    public void setIsTaskFinished(Boolean isTaskFinished) {
        this.isTaskFinished = isTaskFinished;
    }

    @Override
    public String toString() {
        return "TaskName: " + taskName +
                System.lineSeparator() +
                "TaskDescription: " + taskDescription +
                System.lineSeparator() +
                "TaskPriority: " + taskPriority +
                System.lineSeparator() +
                "StartDate: " + taskStartDate +
                System.lineSeparator() +
                "EndDate: " + taskEndDate +
                System.lineSeparator() +
                "Deadline: " + deadline +
                System.lineSeparator() +
                "Category: " + category +
                System.lineSeparator() +
                "Finished?: " + isTaskFinished +
                System.lineSeparator() +
                "ID: " + taskIDNumber;
    }
}
