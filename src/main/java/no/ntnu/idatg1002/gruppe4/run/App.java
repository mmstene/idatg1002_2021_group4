package no.ntnu.idatg1002.gruppe4.run;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import no.ntnu.idatg1002.gruppe4.task.Task;
import no.ntnu.idatg1002.gruppe4.task.TaskList;
import no.ntnu.idatg1002.gruppe4.task.TaskListDaoDB;
import java.util.List;
import java.util.Optional;

/**
 * The App.
 */
public class App extends Application {

    /**
     * The constant taskList containing all tasks in a database.
     * Changing the testing parameter to 'true' will change the db to the test-db in the GUI.
     * Setting it to true, and running tests twice will clear the db. (Remember to change it back to false).
     */
    public static TaskList taskList = new TaskListDaoDB(false);

    /**
     * The Tasks currently shown in the list view. Used to get Task objects based on index from a list view.
     */
    public static List<Task> tasksCurrentlyShown;

    /**
     * String containing previous view. Used to return from settings to other views.
     */
    public static String previousView;

    /**
     * Holds the index of the selected task a listView
     */
    public static int selectedIndex;

    /**
     * Gets selected index.
     *
     * @return the selected index
     */
    public static int getSelectedIndex() {
        return selectedIndex;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Confirmation alert when exiting the application
     */
    public static void exitDialogue(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit application");
        alert.setHeaderText("Do you really want to exit the application?");
        alert.setContentText("Are you sure you want to exit?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            System.exit(0);
        } else {
            alert.close();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.
                load((getClass().getClassLoader().getResource("MainMenuView.fxml")));
        primaryStage.setTitle("FastTasks");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images/newTask.png")));
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}