package no.ntnu.idatg1002.gruppe4.task;

import java.util.List;

/**
 * The interface task list.
 */
public interface TaskList {

    /**
     * Adds a new task
     *
     * @param task the new task
     */
    void addTask(Task task);

    /**
     * Removes a task
     *
     * @param task the task to be removed
     */
    @SuppressWarnings("unused")
    void removeTask(Task task);

    /**
     * Updates a task
     *
     * @param task the task to be updated
     */
    void updateTask(Task task);

    /**
     * Marks a task as done
     *
     * @param task the task to be finished
     */
    void markAsDone(Task task);

    /**
     * Unmarks a task as done
     *
     * @param task the task to be unfinished
     */
    void unmarkAsDone(Task task);

    /**
     * Removes all tasks from database
     */
    void purgeAll();

    /**
     * Gets a task by its id
     *
     * @param id the task id
     *
     * @return the task
     */
    Task getTaskById(int id);

    /**
     * Gets a list of all unique categories
     *
     * @return the list of categories
     */
    List<String> getCategories();

    /**
     * Gets a list of all active tasks (tasks that are not finished)
     *
     * @return the list of tasks in the active task list
     */
    List<Task> getActiveTaskList();

    /**
     * Gets a list of all archived tasks (tasks that are finished)
     *
     * @return the list of tasks in the archive
     */
    List<Task> getArchivedTaskList();

    /**
     * Gets a list of all active tasks filtered by priority
     *
     * @param priority the priority of tasks to be returned
     *
     * @return the list of tasks that have been filtered
     */
    List<Task> getTasksFilteredByPriority(String priority);

    /**
     * Gets a list of all active tasks filtered by category
     *
     * @param category the category of tasks to be returned
     *
     * @return the list of tasks that have been filtered
     */
    List<Task> getTasksFilteredByCategory(String category);

    /**
     * Gets a list of all active tasks filtered by priority and category
     *
     * @param priority the priority of tasks to be returned
     * @param category the category of tasks to be returned
     *
     * @return the list of tasks that have been filtered
     */
    List<Task> getTasksByFilter(String priority, String category);

    /**
     * Closes the Jakarta Persistence entityManager and entityManagerFactory
     */
    void close();
}
