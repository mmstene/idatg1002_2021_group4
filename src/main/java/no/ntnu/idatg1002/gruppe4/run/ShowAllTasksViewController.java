package no.ntnu.idatg1002.gruppe4.run;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import no.ntnu.idatg1002.gruppe4.task.Task;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Show all tasks view controller.
 */
public class ShowAllTasksViewController implements Initializable {

    @FXML
    private Button backButton;
    @FXML
    private Button settingsButton;
    @FXML
    private ComboBox<String> sortByButton;
    @FXML
    private ComboBox<String> sortByPriority;
    @FXML
    private ComboBox<String> filterByCategory;
    @FXML
    private ListView listOfTasks;
    @FXML
    private TextField title;
    @FXML
    private TextField priority;
    @FXML
    private TextField category;
    @FXML
    private TextField startDate;
    @FXML
    private TextField deadline;
    @FXML
    private TextArea description;

    /**
     * Gets combo box sort by.
     *
     * @param actionEvent the action event
     * @return the combo box sort by
     */
    @FXML
    public ComboBox getComboBoxSortBy(ActionEvent actionEvent) {
        return sortByButton;
    }

    /**
     * Gets combo box priority.
     *
     * @param actionEvent the action event
     * @return the combo box priority
     */
    @FXML
    public ComboBox getComboBoxPriority(ActionEvent actionEvent) {
        return sortByPriority;
    }

    /**
     * Gets combo box category.
     *
     * @param actionEvent the action event
     * @return the combo box category
     */
    @FXML
    public ComboBox getComboBoxCategory(ActionEvent actionEvent) {
        return filterByCategory;
    }

    /**
     * Handle back button. Goes back to main menu.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleBackButton() throws IOException {
        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("MainMenuView.fxml")));
        Stage window = (Stage) backButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Handle reset button. Resets filtering and sorting to default view.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleResetButton() throws IOException {
        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("ShowAllTasksView.fxml")));
        Stage window = (Stage) listOfTasks.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Handle settings button. Opens settings.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleSettingsButton() throws IOException {
        App.previousView = "ShowAllTasksView.fxml";

        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("SettingsView.fxml")));
        Stage window = (Stage) settingsButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle exit button. Exit the program.
     */
    @FXML
    public void handleExitButton(){
        App.exitDialogue();
    }

    /**
     * Update list view with tasks in App.tasksCurrentlyShown
     */
    public void updateListView() {
        ObservableList<String> listOfObservableTaskNames = FXCollections.observableArrayList(new ArrayList<>());
        for (Task task : App.tasksCurrentlyShown) {
            listOfObservableTaskNames.add(task.getTaskName());
        }
        listOfTasks.setItems(listOfObservableTaskNames);
    }

    /**
     * Gets all active tasks and sets them to App.tasksCurrentlyShown
     */
    public void getAllActiveTasks() {
        App.tasksCurrentlyShown = App.taskList.getActiveTaskList();
        // Update list view with new tasks
        updateListView();
    }

    /**
     * Handles filtering and sorting of tasks shown.
     * <p>
     * Firstly sets App.tasksCurrentlyShown to a new empty arrayList.
     * Secondly gets tasks from database based on filtering selected and adds these to App.tasksCurrentlyShown.
     * Thirdly sorts App.tasksCurrentlyShown based on sorting selected.
     * Finally updates the listview.
     */
    public void handleFiltering() {
        //List<Task> tasksCurrentlyShown = new ArrayList<>();
        App.tasksCurrentlyShown = new ArrayList<>();
        // No filtering
        if(filterByCategory.getValue() == null && sortByPriority.getValue() == null) {
            App.tasksCurrentlyShown.addAll(App.taskList.getActiveTaskList());
        // Filtering only by priority
        } else if(filterByCategory.getValue() == null && sortByPriority.getValue() != null) {
            App.tasksCurrentlyShown.addAll(App.taskList.getTasksFilteredByPriority(sortByPriority.getValue()));
        // Filter only by category
        } else if(filterByCategory.getValue() != null && sortByPriority.getValue() == null) {
            App.tasksCurrentlyShown.addAll(App.taskList.getTasksFilteredByCategory(filterByCategory.getValue()));
        // Filter by category and priority
        } else {
            App.tasksCurrentlyShown.addAll(App.taskList.getTasksByFilter(sortByPriority.getValue(), filterByCategory.getValue()));
        }
        // Sorting
        if(sortByButton.getValue() != null) {
            switch (sortByButton.getValue()) {
                case "Deadline":
                    App.tasksCurrentlyShown = App.tasksCurrentlyShown.stream()
                            .sorted(Comparator.comparing(Task::getDeadline, Comparator.naturalOrder()))
                            .collect(Collectors.toList());
                    break;
                case "Priority (High-Low)":
                    App.tasksCurrentlyShown = App.tasksCurrentlyShown.stream()
                            .sorted(Comparator.comparing(Task::getTaskPriorityNumeric, (priority1, priority2) -> priority2 - priority1))
                            .collect(Collectors.toList());
                    break;
                case "Priority (Low-High)":
                    App.tasksCurrentlyShown = App.tasksCurrentlyShown.stream()
                            .sorted(Comparator.comparingInt(Task::getTaskPriorityNumeric))
                            .collect(Collectors.toList());
                    break;
                case "Alphabetical (A-Z)":
                    App.tasksCurrentlyShown = App.tasksCurrentlyShown.stream()
                            .sorted(Comparator.comparing(Task::getTaskName))
                            .collect(Collectors.toList());
                    break;
                case "Alphabetical (Z-A)":
                    App.tasksCurrentlyShown = App.tasksCurrentlyShown.stream()
                            .sorted(Comparator.comparing(Task::getTaskName).reversed())
                            .collect(Collectors.toList());
                    break;
                default:
                    break;
            }
        }
        updateListView();
    }

    /**
     * Displays info about selected task.
     *
     * @param click a mouse click
     * @throws IOException the io exception
     */
    @FXML
    void displaySelected(MouseEvent click) throws IOException {
        if(listOfTasks.getSelectionModel().getSelectedItem() != null && click.getClickCount() == 2) {
            Parent root = FXMLLoader.
                    load((getClass().getClassLoader().getResource("EditTaskView.fxml")));

            Stage window = (Stage) listOfTasks.getScene().getWindow();
            window.setScene(new Scene(root));
        }
        else if(listOfTasks.getSelectionModel().getSelectedItem() != null) {
            // Index selected in task view
            int tempIndex = listOfTasks.getSelectionModel().getSelectedIndex();
            // Get ID of Task based on tempIndex
            App.selectedIndex = App.tasksCurrentlyShown.get(tempIndex).getTaskIDNumber();
            Task taskToBeDisplayed = App.taskList.getTaskById(App.getSelectedIndex());
            title.setText(taskToBeDisplayed.getTaskName());
            priority.setText(taskToBeDisplayed.getTaskPriority());
            category.setText(taskToBeDisplayed.getCategory());
            startDate.setText(taskToBeDisplayed.getTaskStartDate().toString());
            deadline.setText(taskToBeDisplayed.getDeadline().toString());
            description.setText(taskToBeDisplayed.getTaskDescription());
        }
    }

    /**
     * Set all unique categories in database to combobox.
     */
    public void getCategories() {
        // Make an observable list with every unique category
        ObservableList<String> listOfCategories = FXCollections.observableArrayList(new ArrayList<>());
        for (String category : App.taskList.getCategories()) {
            listOfCategories.add(category);
        }
        // Set category combobox to every unique existing category
        filterByCategory.setItems(listOfCategories);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        getAllActiveTasks();
        getCategories();
    }
}