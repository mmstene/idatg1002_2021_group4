package no.ntnu.idatg1002.gruppe4.run;

public class AppLauncher {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args){
        App.main(args);
    }
}
