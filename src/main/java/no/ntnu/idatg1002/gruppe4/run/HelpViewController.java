package no.ntnu.idatg1002.gruppe4.run;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HelpViewController implements Initializable{

    public Button backButton;

    /**
     * Handle back button. Returns to MainView
     *
     * @throws IOException
     */
    @FXML
    public void handleBackButton() throws IOException {
        Parent root = FXMLLoader.
                load((getClass().getClassLoader().getResource("SettingsView.fxml")));
        Stage window = (Stage) backButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle exit button. exits the application
     */
    @FXML
    public void handleExitButton(){
        App.exitDialogue();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
