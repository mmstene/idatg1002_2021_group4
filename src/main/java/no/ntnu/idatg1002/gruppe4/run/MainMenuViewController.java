package no.ntnu.idatg1002.gruppe4.run;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import no.ntnu.idatg1002.gruppe4.task.Task;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * The Main menu view controller.
 */
public class MainMenuViewController implements Initializable {
    @FXML
    private ListView listOfTasks;
    @FXML
    private Button settingsButton;
    @FXML
    private Button newTaskButton;
    @FXML
    private Button gotoActiveTasksButton;
    @FXML
    private Button gotoArchivedTasksButton;
    @FXML
    private Button markAsDoneButton;

    /**
     * Handle settings button. Goes to settings.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleSettingsButton() throws IOException {
        App.previousView = "MainMenuView.fxml";

        Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("SettingsView.fxml")));
        Stage window = (Stage) settingsButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle exit button. Exits the program.
     */
    @FXML
    public void handleExitButton(){
        App.exitDialogue();
    }

    /**
     * Handle new task button. Goes to new task view.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleNewTaskButton() throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("NewTaskView.fxml"));
        Stage window = (Stage) newTaskButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle archive button. Goes to archived tasks view.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleArchiveButton() throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("ArchivedTasksView.fxml"));
        Stage window = (Stage) gotoArchivedTasksButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle active tasks button. Goes to active tasks view.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleActiveTasksButton() throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("ShowAllTasksView.fxml"));
        Stage window = (Stage) gotoActiveTasksButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle mark as done. Marks a task as done.
     *
     * @throws IOException the io exception
     */
    @FXML
    public void handleMarkAsDone() throws IOException {
        if(listOfTasks.getSelectionModel().getSelectedItem() != null) {
            ObservableList<Integer> indices = listOfTasks.getSelectionModel().getSelectedIndices();
            Task selectedTask;
            for(Integer index : indices) {
                // Get the task of selected index
                selectedTask = App.tasksCurrentlyShown.get(index);
                // Mark the task as finished
                App.taskList.markAsDone(selectedTask);
            }
            updateTasks();
            markAsDoneButton.setDisable(true);
        }
    }

    /**
     * Updates global list of tasks currently shown from database. Adds every task shown to list view.
     */
    public void updateTasks() {
        // Get tasks from database
        App.tasksCurrentlyShown = App.taskList.getActiveTaskList();
        // Set tasks to list view
        ObservableList<String> listOfTaskNames = FXCollections.observableArrayList(new ArrayList<>());
        for (Task task : App.tasksCurrentlyShown) {
            listOfTaskNames.add(task.getTaskName());
        }
        listOfTasks.setItems(listOfTaskNames);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateTasks();
        listOfTasks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Opens EditTaskView for selected task.
     *
     * @param click the event
     * @throws IOException the io exception
     */
    @FXML
    void displaySelectedTask(MouseEvent click) throws IOException {
        if(listOfTasks.getSelectionModel().getSelectedItem() != null && click.getClickCount() == 2) {
            // Index selected in task view
            int tempIndex = listOfTasks.getSelectionModel().getSelectedIndex();
            // Get ID of Task based on tempIndex
            App.selectedIndex = App.tasksCurrentlyShown.get(tempIndex).getTaskIDNumber();
            Parent root = FXMLLoader.load((getClass().getClassLoader().getResource("EditTaskView.fxml")));
            Stage window = (Stage) listOfTasks.getScene().getWindow();
            window.setScene(new Scene(root));
        }
        else if(listOfTasks.getSelectionModel().getSelectedItem() != null)
            markAsDoneButton.setDisable(false);
        else
            markAsDoneButton.setDisable(true);
    }
}
