module no.ntnu.idatg1002.gruppe4 {
    requires javafx.controls;
    requires javafx.fxml;
    requires jakarta.persistence;
    // Issue which seems to be caused by a bug outside of our control.
    requires eclipselink;
    requires org.apache.derby.engine;
    requires org.apache.derby.commons;

    opens no.ntnu.idatg1002.gruppe4.run to javafx.fxml;
    opens no.ntnu.idatg1002.gruppe4.task to eclipselink;

    exports no.ntnu.idatg1002.gruppe4.run;
    exports no.ntnu.idatg1002.gruppe4.task;
}