import no.ntnu.idatg1002.gruppe4.task.Task;
import no.ntnu.idatg1002.gruppe4.task.TaskListDaoDB;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

/**
 * NOTE: to run the tests you NEED to run it trough MAVEN (on the right, press MAVEN,
 * then double-click TEST under LIFECYCLE
 */
class TaskListTest {

    /**
     * The Task list dao db.
     */
    TaskListDaoDB taskListDaoDB = new TaskListDaoDB(true);

    /**
     * Sets up test.
     */
    @BeforeEach
    @DisplayName("Creates some tasks to fill into the active tasks list prior to every test")
    void setUpTest() {
        Task taskA = new Task("TaskA", "DesA","Low", LocalDate.now(), "Cat1");
        Task taskB = new Task("TaskB", "DesB","Low", LocalDate.now(), "Cat1");
        Task taskC = new Task("TaskC", "DesC","Medium", LocalDate.now(), "Cat2");
        taskListDaoDB.addTask(taskA);
        taskListDaoDB.addTask(taskB);
        taskListDaoDB.addTask(taskC);
    }

    /**
     * Gets active task list test.
     */
    @Test
    @DisplayName("Test to make sure that the active list is created")
    void getActiveTaskListTest() {
        assertTrue(taskListDaoDB.getActiveTaskList().size() >= 0);

    }

    /**
     * Gets archived task list test.
     */
    @Test
    @DisplayName("Test to make sure that the archived list is created")
    void getArchivedTaskListTest() {
        assertTrue(taskListDaoDB.getArchivedTaskList().size() >= 0);
    }

    /**
     * Add task test.
     */
    @Test
    @DisplayName("Adds a task to the active task list")
    void addTaskTest() {
       Task task2 = new Task("Task2", "Des2","High", LocalDate.now(), "Cat2");
        taskListDaoDB.addTask(task2);
        assertEquals(4, taskListDaoDB.getActiveTaskList().size());
    }

    /**
     * Remove task test.
     */
    @Test
    @DisplayName("Removes a task from the active task list")
    void removeTaskTest() {
        Task task2 = new Task("Task2", "Des2","High", LocalDate.now(), "Cat2");
        taskListDaoDB.addTask(task2);
        taskListDaoDB.removeTask(task2);
        assertEquals(3, taskListDaoDB.getActiveTaskList().size());
    }

    /**
     * Update task test.
     */
    @Test
    @DisplayName("Updates a task (in the active task list) based on user input")
    void updateTaskTest() {
        // Duplicate to test every setter.
        //One task need to be in the test for it to work.
        taskListDaoDB.getActiveTaskList().get(0).setTaskDescription("ChangedDes2");
        assertEquals("ChangedDes2", taskListDaoDB.getActiveTaskList().get(0).getTaskDescription());
    }

    /**
     * Mark as done test.
     */
    @Test
    @DisplayName("Marks a task as done.")
    void markAsDoneTest() {
        Task task2 = new Task("Task2", "Des2","High", LocalDate.now(), "Cat2");
        taskListDaoDB.addTask(task2);
        taskListDaoDB.markAsDone(task2);
        assertEquals(3, taskListDaoDB.getActiveTaskList().size());
        assertEquals(1, taskListDaoDB.getArchivedTaskList().size());
        assertEquals(true, taskListDaoDB.getArchivedTaskList().get(0).getTaskFinished());
    }

    /**
     * Gets categories test.
     */
    @Test
    @DisplayName("Displays all the categories that exists")
    void getCategoriesTest() {
        List listOfCategories = new ArrayList();
        listOfCategories.add("Cat1");
        listOfCategories.add("Cat2");
        //Not quite the same, but the content is close enough to give a match (different type of lists..?)
        assertEquals(listOfCategories, taskListDaoDB.getCategories());

    }


    /**
     * Gets tasks filtered by priority test.
     */
    @Test
    @DisplayName("Filters a list based on chosen priority")
    void getTasksFilteredByPriorityTest() {
        Task taskAA = new Task("TaskAA", "DesA","Low", LocalDate.now(), "Cat1");
        Task taskBB = new Task("TaskBB", "DesB","Low", LocalDate.now(), "Cat3");
        Task taskCC = new Task("TaskCC", "DesC","High", LocalDate.now(), "Cat2");
        taskListDaoDB.addTask(taskAA);
        taskListDaoDB.addTask(taskBB);
        taskListDaoDB.addTask(taskCC);
        // list is, unchanged, with lowest index on the left; A, B, C, AA, BB, CC.
        assertNotEquals(taskCC, taskListDaoDB.getActiveTaskList().get(0));
        assertEquals(taskCC, taskListDaoDB.getTasksFilteredByPriority("High").get(0));


    }

    /**
     * Gets tasks filtered by category test.
     */
    @Test
    @DisplayName("Filters a list based on chosen category")
    void getTasksFilteredByCategoryTest() {
        Task taskAA = new Task("TaskAA", "DesA","Low", LocalDate.now(), "Cat1");
        Task taskBB = new Task("TaskBB", "DesB","Low", LocalDate.now(), "Cat3");
        Task taskCC = new Task("TaskCC", "DesC","High", LocalDate.now(), "Cat2");
        taskListDaoDB.addTask(taskAA);
        taskListDaoDB.addTask(taskBB);
        taskListDaoDB.addTask(taskCC);
        // list is, unchanged, with lowest index on the left; A, B, C, AA, BB, CC.
        assertNotEquals(taskBB, taskListDaoDB.getActiveTaskList().get(0));
        assertEquals(taskBB, taskListDaoDB.getTasksFilteredByCategory("Cat3").get(0));
    }

    /**
     * Gets tasks by filter test.
     */
    @Test
    @DisplayName("Filters a list based on chosen priority and category")
    void getTasksByFilterTest() {
        Task taskAA = new Task("TaskAA", "DesA","Low", LocalDate.now(), "Cat1");
        Task taskBB = new Task("TaskBB", "DesB","Low", LocalDate.now(), "Cat3");
        Task taskCC = new Task("TaskCC", "DesC","High", LocalDate.now(), "Cat2");
        taskListDaoDB.addTask(taskAA);
        taskListDaoDB.addTask(taskBB);
        taskListDaoDB.addTask(taskCC);
        // list is, unchanged, with lowest index on the left; A, B, C, AA, BB, CC.
        assertNotEquals(taskBB, taskListDaoDB.getActiveTaskList().get(0));
        assertEquals(taskBB, taskListDaoDB.getTasksByFilter("Low", "Cat3").get(0));
    }

    /**
     * Wipe the db clean.
     */
// To see the tests in GUI, run the comment out the @AfterEach method below, then run the test once,
    // finally run the program.
    @AfterEach
    @DisplayName("Cleans the database after each test")
    void wipeTheDBClean() {
        for (Task task: taskListDaoDB.getActiveTaskList()) {
            taskListDaoDB.removeTask(taskListDaoDB.getActiveTaskList().get(0));
        }
        for (Task task: taskListDaoDB.getArchivedTaskList()) {
            taskListDaoDB.removeTask(taskListDaoDB.getArchivedTaskList().get(0));
        }
    }
}