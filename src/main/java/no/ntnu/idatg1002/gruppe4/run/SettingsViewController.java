package no.ntnu.idatg1002.gruppe4.run;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class SettingsViewController implements Initializable {
    public Button backButton;
    public Button exitButton;
    public Button contactUsButton;
    public Button helpButton;
    public Button deleteButton;

    /**
     * Handle back button. Returns to MainView
     *
     * @throws IOException
     */
    @FXML
    public void handleBackButton() throws IOException {
        Parent root = FXMLLoader.
                load((getClass().getClassLoader().getResource(App.previousView)));

        Stage window = (Stage) backButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handle Help button. Goes to the help menu
     *
     * @throws IOException
     */
    @FXML
    public void handleHelpButton() throws IOException {
        Parent root = FXMLLoader.
                load((getClass().getClassLoader().getResource("HelpView.fxml")));

        Stage window = (Stage) helpButton.getScene().getWindow();
        window.setScene(new Scene(root, 1000, 600));
    }

    /**
     * Handles the delete all tasks button. Deletes all tasks from database.
     */
    @FXML
    public void handleDeleteButton() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete everything");
        alert.setHeaderText("Do you really want to delete all the tasks?");
        alert.setContentText("Are you sure you want to delete all the tasks?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            App.taskList.purgeAll();
        } else {
            alert.close();
        }
    }

    /**
     * Handle contact button. creates a popup with info about how to contact us.
     * @throws IOException
     */
    @FXML
    public void handleContactButton() throws IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Contact us");
        alert.setHeaderText(null);
        String s ="If you have any questions or feedback";
        alert.setContentText(s + "you can email us at ane.lo@live.no");
        alert.show();
    }

    /**
     * Handle exit button. exits the application
     */
    @FXML
    public void handleExitButton(){
        App.exitDialogue();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
