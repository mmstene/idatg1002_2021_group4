# Table of Contents

1. [Description](#)
2. [Installation](#)
3. [Usage](#)
4. [Credits](#)
5. [License](#)




# Description
This is a school project for NTNU Software engineering course IDATG1002!
On this project we are 6 members, who are as following Espen Jønsson, Kristian Rønning, Martin Stene, Ole Anders Hvalsmarken, Jonas Mørkland and Ane Lo

The project is called FastTask and is meant to let a person have more controll over their everyday life.

We used FXML files to have more controll over the GUI part of the application, and so the code looks more clean.
We choose to go with a local database instead of a local file on your computer, to make the application more clean and easier to make changes on.

The program is written in Java and using JavaFX for GUI.

We faced many small challenges along the way, including struggles to create a object when clicking the create task button,
we also faced some major challenges including testing, where we spent many days figuring out how we did test with a database. What we ended up doing was creating a second database so we can see the test tasks in another database and not mix the test tasks and normal task the user has.

# Installation

This installation manual is a guide to download and install the program "FastTasks". The manual will also clarify the requirements to be able to run it.

REQUIREMENTS

The application requires 22.1 MB of free space on your computer.

To be able to run this program you have to have Java Runtime Environment 14 installed on the computer.
This can be downloaded from the following page: https://www.oracle.com/java/technologies/javase/jdk14-archive-downloads.html

Follow the instructions given on this page to install Java Runtime Enviroment 14.

INSTALLATION

[Download the .jar-file here](https://gitlab.stud.iie.ntnu.no/mmstene/idatg1002_2021_group4/-/blob/master/FastTasks.jar) 

Locate to where you downloaded the ZIP, and extract the .jar-file in a location you can easily find it later, for instance in your desktop folder.

RUNNING THE PROGRAM

To run the program, locate to where you extracted the .jar-file and double-click on it.

OR

Open any terminal, for instance Command prompt, and write: _java -jar "PATH"_ (where PATH is the absolute path of the .jar-file).

Press the Enter key.

# Usage
FastTask is used as a Laptop or desktop application and can be but not limited to used as an application to make tasks to do in the near future or many years in advance. When done you can mark the tasks as done and they will be archived.

You can use this to keep track of what needs to be done on different thing for example a code you are working on, you set the task and you can mark the task as done when you have finished coding it.

You can also use it as a bucketlist of the things you wanna do most in life, and mark them as done when you have done them, Which will then archive them and you can go back to look at when you have done them!

If you need a instruction on how to use the program, please refer to Help tab in the program 
Which can be accessed if you go to the settings(The Cogs on top middle of the application) -> Help 

# Credits
All the credits goes to our different teachers! 

Kiran Bylappa Raja(IDATG2001 teacher) - For helping us if we had problems with the code, and publising some tutorials on how to do different things.

Seyed Ali Amirshahi(IDATG1002 teacher) - For teaching us the different techniques in the Software engineering trade, and also helping clear things up when we got confused.

# License
Copyright (c) 2021 FastTask application

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the FastTask), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

